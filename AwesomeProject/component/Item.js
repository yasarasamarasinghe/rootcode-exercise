/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {TouchableOpacity, Text, Image, StyleSheet, View} from 'react-native';

function Item({
  id,
  trackName,
  collectionName,
  artistName,
  artworkUrl,
  onPressCard,
}) {
  return (
    <TouchableOpacity
      onPress={() =>
        onPressCard({trackName, collectionName, artistName, artworkUrl})
      }
      style={{flex: 1, flexDirection: 'row', padding: 2}}>
      <View
        style={{
          width: '40%',
          height: 100,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image
          style={styles.tinyLogo}
          source={{
            uri: artworkUrl,
          }}
        />
      </View>
      <View
        style={{
          width: '60%',
          height: 100,
          justifyContent: 'flex-start',
          padding: 5,
        }}>
        <Text style={styles.track}>{trackName}</Text>
        <Text style={styles.track}>{collectionName}</Text>
        <Text style={styles.track}>{artistName}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  track: {
    fontSize: 15,
  },
  tinyLogo: {
    height: '70%',
    width: '70%',
    resizeMode: 'contain',
  },
});

export default Item;
