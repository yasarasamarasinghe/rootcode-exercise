/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import axios from 'axios';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
  Image,
  Modal,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';

import Item from './component/Item';

import {SearchBar} from 'react-native-elements';

const itunesUrl = 'https://itunes.apple.com';

export default class App extends React.Component {
  state = {
    search: '',
    results: null,
    modalVisible: false,
    selectedItem: null,
  };

  updateSearch = search => {
    this.setState({search});
    console.log(this.state.search);

    axios
      .get(itunesUrl + '/search?term=' + search)
      .then(response => {
        // handle success
        if (response.data.results) {
          let {results} = response.data;

          // Limit to 25 Records
          results = results.slice(0, 25);
          this.setState({results});
        } else {
          this.setState({results: null});
        }
      })
      .catch(function(error) {
        // handle error
        console.log(error);
      });
  };

  setModalVisible = visible => {
    this.setState({modalVisible: visible});
  };

  render() {
    const {search, results, selectedItem} = this.state;

    return (
      <>
        <SearchBar
          placeholder="Search Here..."
          onChangeText={this.updateSearch}
          value={search}
        />

        <SafeAreaView style={styles.container}>
          {results ? (
            <FlatList
              data={results}
              renderItem={({item}) => (
                <Item
                  id={item.trackId}
                  trackName={item.trackName}
                  collectionName={item.collectionName}
                  artistName={item.artistName}
                  artworkUrl={item.artworkUrl100}
                  onPressCard={item => {
                    this.setModalVisible(true);
                    this.setState({selectedItem: item});
                  }}
                />
              )}
            />
          ) : (
            <View />
          )}
        </SafeAreaView>

        <Modal
          animationType={'slide'}
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            console.log('Modal has been closed.');
          }}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            {selectedItem ? (
              <>
                <View
                  style={{
                    width: '40%',
                    height: 100,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <Image
                    style={styles.tinyLogo}
                    source={{
                      uri: selectedItem.artworkUrl,
                    }}
                  />
                </View>
                <Text style={styles.track}>{selectedItem.trackName}</Text>
                <Text style={styles.track}>{selectedItem.collectionName}</Text>
                <Text style={styles.track}>{selectedItem.artistName}</Text>
              </>
            ) : (
              <Text>No Data</Text>
            )}

            <TouchableHighlight
              onPress={() => {
                this.setModalVisible(false);
              }}>
              <Text style={styles.text}>Close Modal</Text>
            </TouchableHighlight>
          </View>
        </Modal>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  track: {
    fontSize: 11,
  },
  tinyLogo: {
    height: '70%',
    width: '70%',
    resizeMode: 'contain',
  },
});
